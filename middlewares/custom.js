const Company = require('../models/company.model');
const { to, ReE } = require('../services/util.service');

let company = async function(req, res, next) {
    let company_id, err, company;
    company_id = req.params.company_id;

    [err, company] = await to(Company.findOne({_id: Company}));
    if (err) return ReE(res, 'error finding company');

    if (!company) return ReE(`Company not found with id: ${company_id}`);

    let user, users;
    user = req.user;
    users = company.users.map(obj => String(obj,user));

    if (!users.includes(String(user._id))) return ReE(`User does not have permission to read company with id: ${company_id}`);

    req.company = company;
    next();
}

module.exports.company = company;