const jwt = require('jsonwebtoken');
const config = require('../config/config');
const { ReE } = require('../services/util.service');
const ADMIN_PERMISSION = require('../config/config').permissions.ADMIN;

const validRefreshNeeded = (req, res, next) => {
    let refresh_token = req.body.refresh_token || req.query.refresh_token || req.headers['refresh_token'] || '';

    if (!refresh_token) {
        return ReE(res, 'No refresh token provided.', 401);
    } else {
        jwt.verify(refresh_token, config.jwt_encryption_refresh, async function (err, user) {
            if (err || !user) {
                return ReE(res, err, 401);
            }
            req.jwt = user;

            return next();
        });
    }
}

const validJWTNeeded = (req, res, next) => {
    let authorization = req.body.token || req.query.token || req.headers['access-token'] || req.headers['authorization'] || '';

    let [tokenType, token] = authorization.trim().split(' ');
    if (tokenType !== 'Bearer') {
        return ReE(res, !authorization ? 'No token provided.' : 'No Bearer token provided.', 401);
    } else {
        jwt.verify(token, config.jwt_encryption, function (err, user) {
            if (err || !user) {
                return ReE(res, err, 401);
            }
            req.jwt = user;

            return next();
        });
    }
}

const requiredPemission = (...permissions) => {
    return (req, res, next) => {
        let userPermission = req.jwt.permissionLevel;
        if (permissions.includes(userPermission)){
            return next();
        }

        return ReE(res, 'Permission denied.', 403);
    }
}

const onlySameUserOrAdminCanDoThisAction = (req, res, next) => {
    let userId = req.jwt.userId;
    let userPermission = req.jwt.permissionLevel;

    if (
        (req.params && req.params.userId && userId == req.params.userId) || 
        userPermission == ADMIN_PERMISSION
    ) {
        return next();
    }

    return ReE(res, 'Permission denied.', 403);
}

const sameUserCantDoThisAction = (req, res, next) => {
    let userId = req.jwt.userId;
    let userPermission = req.jwt.permissionLevel;

    if (req.params && req.params.userId && userId != req.params.userId) {
        return next();
    }

    return ReE(res, 'Permission denied.', 403);
}

module.exports = {
    validations: {
        validJWTNeeded: validJWTNeeded,
        validRefreshNeeded: validRefreshNeeded,
    },
    permissions: {
        requiredPemission: requiredPemission,
        onlySameUserOrAdminCanDoThisAction: onlySameUserOrAdminCanDoThisAction,
        sameUserCantDoThisAction: sameUserCantDoThisAction,
    }
}