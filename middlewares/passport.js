const { ExtractJwt, Strategy } = require('passport-jwt');
const { User } = require('../models');
const { to } = require('../services/util.service');
const config = require('../config/config');

module.exports = function(passport) {
    let opts = {};

    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = config.jwt_encryption;

    console.log(opts);
    passport.use(new Strategy(opts, async function(payload, done){
        console.log(payload);
        let [err, user] = await to(User.findById(payload.id));
        if (err) return done(err, false);

        return user ? done(null, user) : done(null, false);
    }));
}