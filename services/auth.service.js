const { User } = require('../models');
const validator = require('validator');
const { to, TE } = require('../services/util.service');

// this is so they can send in 3 options unique_key, email, or phone and it will work
const getUniqueKeyFromBody = function (body) {
    let uniqueKey = body.unique_key;
    if (!uniqueKey) {
        if (body.email) {
            uniqueKey = body.email
        } else if (body.phone) {
            uniqueKey = body.phone
        } else {
            uniqueKey = null;
        }
    }

    return uniqueKey;
}

const createUser = async function (userInfo) {
    let uniqueKey, authInfo, err;

    authInfo = {}
    authInfo.status = 'create';

    uniqueKey = getUniqueKeyFromBody(userInfo);
    if (!uniqueKey) TE('An email or phone number was not entered.');

    if (validator.isEmail(uniqueKey)) {
        authInfo.method = 'email';
        userInfo.email = uniqueKey;

        [err, user] = await to(User.create(userInfo));
        if (err) TE('user already exists with that email');

        return user;

    } else if (validator.isMobilePhone(uniqueKey, 'any')) {
        authInfo.method = 'phone';
        userInfo.phone = uniqueKey;

        [err, user] = await to(User.create(userInfo));
        if (err) TE('user already exists with that phone number');

        return user;
    } else {
        TE('A valid email or phone number was not entered.');
    }
}

const authUser = async function (userInfo) {
    let uniqueKey;
    let authInfo = {};
    authInfo.status = 'login';
    uniqueKey = getUniqueKeyFromBody(userInfo);

    if (!uniqueKey) TE('Please enter an email or phone number to login');

    if (!userInfo.password) TE('Please enter a password to login');

    let err, user;
    if (validator.isEmail(uniqueKey)) {
        authInfo.method = 'email';

        [err, user] = await to(User.findOne({ email: uniqueKey }));
        if (err) TE(err.message);

    } else if (validator.isMobilePhone(uniqueKey, 'any')) {
        authInfo.method = 'phone';

        [err, user] = await to(User.findOne({ phone: uniqueKey }));
        if (err) TE(err.message);

    } else {
        TE('A valid email or phone number was not entered');
    }

    if (!user) TE('User not registered.');

    [err, user] = await to(user.comparePassword(userInfo.password));
    if (err) TE(err.message);

    return user;
}

const refreshAuthUser = async function (userId) {
    if (!userId) TE('No user provided.');

    let [err, user] = await to(User.findById(userId));
    if (err) TE(err.message);

    return user;
}

module.exports = {
    createUser: createUser,
    authUser: authUser,
    refreshAuthUser: refreshAuthUser,
    getUniqueKeyFromBody: getUniqueKeyFromBody
}