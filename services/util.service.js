const { to } = require('await-to-js');
const pe = require('parse-error');

module.exports.to = async (promise) => {
    [err, res] = await to(promise);
    if (err) return [pe(err)];

    return [null, res];
}

// response error
module.exports.ReE = function (res, err, code) {
    let results = { success: false, error: err };
    if (typeof err === 'object' && err.message) {
        results.message = err.message;
    }

    if (code) {
        res.statusCode = code;
    }

    return res.json(results);
}

// response success
module.exports.ReS = function (res, data, code) {
    let results = {success: true};

    if (typeof data === 'object') {
        results = Object.assign(data, results);
    } else {
        results.data = data;
    }

    res.statusCode = code || 200;

    return res.json(results);
}

// throwing error
module.exports.TE = function(err, log) {
    if (log) {
        console.error(err);
    }

    throw new Error(err);
}