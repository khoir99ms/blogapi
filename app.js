const express       = require('express');
const logger        = require('morgan');
const bodyParser    = require('body-parser');
const passport      = require('passport');
const pe            = require('parse-error');
const cors          = require('cors');

const app       = express();
const v1        = require('./routes/v1.route');
const config    = require('./config/config');
const models    = require('./models');

app.use(cors());
app.use(logger('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());

app.use('/api/v1.0', v1);

app.get('/', function (req, res) {
    res.status(200);
    res.json({ status: "success", message: "Blog API" })
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err       = new Error('Not Found');
    err.status    = 404;

    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        errors: {
            message: err.message,
            error: config.env === 'development' ? err : {}
        },
    });
});

module.exports = app;

process.on('unhandledRejection', error => {
    console.log('Uncaught Error', pe(error));
});