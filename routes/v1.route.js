const express = require('express');
/* const passport = require('passport');
const jwt = require('jsonwebtoken');
const { to, ReE } = require('../services/util.service');
const config = require('../config/config'); */

// const HomeController = require('../controllers/home.controller');

/* const { User } = require('../models');
const UserController = require('../controllers/user.controller');
const CompanyController = require('../controllers/company.controller'); */

// const custom = require('../middleware/custom');

// require('../middleware/passport')(passport);

// const authorized = function (req, res, next) {
//     let authorization = req.body.token || req.query.token || req.headers['access-token'] || req.headers['authorization'] || '';
    // console.log(authorization);
    // token = ExtractJwt.fromAuthHeaderAsBearerToken();
    // console.log('ExtractJwt -------> ' + token);
    // console.log(req.headers);

    /* if (authorization && authorization.split(' ')[0] === 'Bearer') {
        authorization = authorization.split(' ')[1];

        // verifies secret and checks exp
        jwt.verify(authorization, config.jwt_encryption, function (error, user) {
            if (error || !user) {
                return res.status(401).json({ "error": true, "message": 'Unauthorized access.', "content": error });
            }
            req.user = user;
            next();
        })(req, res, next);
    } else {
        passport.authenticate('jwt', { session: false, }, async (error, user) => {
            if (error || !user) {
                return res.status(401).json({ message: 'Unauthorized access' });
            }
            req.user = user;
            next();
        })(req, res, next);
    } */
    /* let [tokenType, token] = authorization.split(' ');
    if (tokenType !== 'Bearer') {
        return res.status(401).send();
    } else {
        jwt.verify(token, config.jwt_encryption, function (err, user) {
            if (err || !user) {
                return ReE(res, err, 401);
            }
            req.jwt = user;
            next();
        });
    }
} */

const router = express.Router();

router.use('/auth', require('./auth.route'));

// router.get('/dash', passport.authenticate('jwt', {session: false}), HomeController.Dashboard);

/* router.post('/users', UserController.create);
router.post('/users/login', UserController.login);

router.get('/users', 
    authorized, 
    // passport.authenticate('jwt', {session: false}), 
    UserController.get
);
router.put('/users', 
    passport.authenticate('jwt', {session: false}), 
    UserController.update
);
router.delete('/users', 
    passport.authenticate('jwt', {session: false}), 
    UserController.remove
);

router.post('/companies', 
    passport.authenticate('jwt', { session: false }), 
    CompanyController.create
);
router.get('/companies', 
    passport.authenticate('jwt', { session: false }), 
    CompanyController.getAll
);

router.get('/companies/:company_id', 
    passport.authenticate('jwt', { session: false }), 
    custom.company, CompanyController.get
);
router.put('/companies/:company_id', 
    passport.authenticate('jwt', { session: false }), 
    custom.company, CompanyController.update
);
router.delete('/companies/:company_id',
    passport.authenticate('jwt', { session: false }), 
    custom.company, CompanyController.remove
); */

module.exports = router;