const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const authController = require('../controllers/auth.controller');

const router = express.Router();

router.post('/', authController.login);

router.post('/refresh', [
    authMiddleware.validations.validRefreshNeeded,
    authController.refreshToken,
]);

router.post('/info', [
    authMiddleware.validations.validJWTNeeded,
    authController.tokenInfo,
]);

module.exports = router;