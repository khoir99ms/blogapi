const mongoose  = require('mongoose');
const validate  = require('mongoose-validator');
const bcrypt    = require('bcrypt');
const bcryptp   = require('bcrypt-promise');
const jwt       = require('jsonwebtoken');
const {TE, to}  = require('../services/util.service');
const Company   = require('./company.model');
const config    = require('../config/config');

let Schema = new mongoose.Schema({
    firstName: { type: String, trim: true },
    lastName: { type: String, trim: true },
    phone: {
        type: String,
        lowercase: true,
        trim: true,
        index: true,
        unique: true,
        sparse: true, //sparse is because now we have two possible unique keys that are optional
        validate: [validate({
            validator: 'isNumeric',
            arguments: [7, 20],
            message: 'Not a valid phone number.',
        })]
    },
    email: {
        type: String,
        lowercase: true,
        trim: true,
        index: true,
        unique: true,
        sparse: true, //sparse is because now we have two possible unique keys that are optional
        validate: [validate({
            validator: 'isEmail',
            message: 'Not a valid email.',
        })],
    },
    password: {type: String}
}, { timestamps: true });

Schema.virtual('fullName')
    .get(function(){
        return [this.firstName, this.lastName].join(' ');
    })
    .set(function (fullName){
        this.firstName = fullName.substr(0, fullName.indexOf(' '));
        this.lastName = fullName.substr(fullName.indexOf(' ') + 1);
    });

Schema.virtual('companies', {
    ref: 'Company',
    localField: '_id',
    foreignField: 'users.user',
    justOne: false
});

Schema.pre('save', async function(next) {
    if(this.isModified('password') || this.isNew){
        let err, salt, hash;
        [err, salt] = await to(bcrypt.genSalt(10));
        if (err) TE(err.message, true);

        [err, hash] = await to(bcrypt.hash(this.password, salt));
        if (err) TE(err.message, true);

        this.password = hash;
    } else {
        return next();
    }
});

Schema.methods.comparePassword = async function(pw) {
    if (!this.password) TE('password not set');

    let [err, password] = await to(bcrypt.compare(pw, this.password));
    if (err) TE(err);

    if (!password) TE('invalid password');

    return this;
}

Schema.methods.Companies = async function() {
    let [err, companies] = await to(Company.find({ 'users.user': this._id }));
    if (err) TE('error getting companies');

    return companies;
}

Schema.methods.getJWT = function () {
    let tokenLife = parseInt(config.jwt_expiration);
    let refreshTokenLife = parseInt(config.jwt_expiration_refresh);

    let token = jwt.sign({
        userId: this._id,
        email: this.email,
        phone: this.phone,
        expire: tokenLife,
    }, config.jwt_encryption, { expiresIn: tokenLife });
    
    let refreshToken = jwt.sign({
        userId: this._id,
        email: this.email,
        phone: this.phone,
        expire: refreshTokenLife,
    }, config.jwt_encryption_refresh, { expiresIn: refreshTokenLife });

    return {
        token: token,
        refreshToken: refreshToken,
    }
}

Schema.methods.toJson = function () {
    let json = this.toJSON();
    json.id = json._id;
    delete json._id;

    return json;
}

module.exports = mongoose.model('User', Schema);