'use strict';

const fs          = require('fs');
const mongo       = require('../config/mongo');
const models = {};

fs
.readdirSync(__dirname)
.filter(file => {
    return /\.model\.js$/.test(file);
})
.forEach((file) => {
    let filename = file.split('.')[0];
    let modelName = filename[0].toUpperCase() + filename.slice(1);
    models[modelName] = require(`./${file}`);
});

module.exports = mongo.connection;
module.exports = models;

