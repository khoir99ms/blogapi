const mongoose = require('mongoose');

let Schema = new mongoose.Schema({
    name: {type: String},
    users: [
        {
            user: { type: mongoose.Schema.ObjectId, res: 'User' },
            permissions: [{type: String}]
        }
    ]
}, {timestamps: true});

Schema.methods.toJson = function(){
    let json = this.toJSON();
    json.id = json._id; //this is for the front end

    return json;
}

module.exports = mongoose.model('Company', Schema);