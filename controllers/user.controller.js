const { to, ReS, ReE } = require('../services/util.service');
const authService = require('../services/auth.service');

const create = async function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    const body = req.body;

    if (!(body.unique_key || body.email || body.phone)){
        return ReE(res, 'Please enter an email or phone number to register.');
    } else if (!body.password) {
        return ReE(res, 'Please enter a password to register.');
    } else {
        let [err, user] = await to(authService.createUser(body));
        if (err) return ReE(res, err, 422);

        return ReS(res, { message: 'Successfully created new user.', user: user.toJson(), token: user.getJWT() }, 201);
    }
}

// the user is returned in req.user from our passport middleware
const get = async function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    let user = req.jwt;
    // return ReS(res, {user: user.toJson()});
    return ReS(res, {user: user});
}

const update = async function(req, res) {
    let err, user, data;
    user = req.user;
    data = req.body;
    user.set(data);

    [err, user] = await to(user.save());
    if (err) {
        console.log(err, user);

        if (err.message.includes('E11000')) {
            if (err.message.includes('phone')) {
                err = 'This phone number is already in use';
            } else if (err.message.includes('email')) {
                err = 'This email address is already in use';
            } else {
                err = 'Duplicate Key Entry';
            }
        }
        return ReE(res, err);
    }

    return ReS(res, { message: 'Updated user ' + user.email })
}

const remove = async function(req, res){
    let user, err;
    user = req.user;

    [err, user] = await to(user.destroy());
    if (err) return ReE(res, 'error occured trying to delete user');

    return ReS(res, { message: 'Deleted user' }, 204);
}

const login = async function(req, res) {
    const body = req.body;
    let [err, user] = await to(authService.authUser(body));
    if (err) return ReE(res, err, 422);

    return ReS(res, { token: user.getJWT(), user: user.toJson() });
}

module.exports = {
    create: create,
    get: get,
    update: update,
    remove: remove,
    login: login,
}