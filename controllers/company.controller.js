const { Company } = require('../models');
const { to, ReE, ReS } = require('../services/util.service');

const create = async function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    let err, company;
    let user = req.user;

    let companyInfo = req.body;
    companyInfo.users = [{ user: user._id }];

    [err, company] = await to(Company.create(companyInfo));
    if (err) return ReE(res, err, 422);

    return ReS(res, { company: company.toJson() }, 201);
}

const getAll = async function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    let user = req.user;
    let err, companies;

    [err, companies] = await to(user.Companies());
    if (err) return ReE(res, err);

    let results = []
    for (let i in companies) {
        let company = companies[i];
        results.push(company.toJson())
    }
    return ReS(res, { companies: results });
}

const get = function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    let company = req.company;

    return ReS(res, { company: company.toJson() });
}

const update = async function (req, res) {
    let err, company, data;
    company = req.user;
    data = req.body;
    company.set(data);
    
    [err, company] = await to(company.save());
    if (err) return ReE(res, err);
    
    return ReS(res, { company: company.toJson() });
}

const remove = async function (req, res) {
    let company, err;
    company = req.company;
    
    [err, company] = await to(company.remove());
    if (err) return ReE(res, 'error occured trying to delete the company');
    
    return ReS(res, { message: 'Deleted Company' }, 204);
}

module.exports = {
    get: get,
    create: create,
    getAll: getAll,
    update: update,
    remove: remove
};