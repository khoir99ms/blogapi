const { User } = require('../models');
const authService = require('../services/auth.service');
const { to, ReS, ReE } = require('../services/util.service');

const login = async function (req, res) {
    const body = req.body;
    let [err, user] = await to(authService.authUser(body));
    if (err) return ReE(res, err, 422);

    return ReS(res, user.getJWT());
}

const refreshToken = async function (req, res) {
    let [err, user] = await to(authService.refreshAuthUser(req.jwt.userId));
    if (err) return ReE(res, err, 422);

    return ReS(res, { token: user.getJWT().token });
}

const tokenInfo = async function (req, res) {
    console.log(req);
    const userId = req.jwt.userId;
    let [err, user] = await to(User.findById(userId));
    if (err) return ReE(res, err, 422);
    
    return ReS(res, { user: user.toJson() });
}

module.exports = {
    login: login,
    refreshToken: refreshToken,
    tokenInfo: tokenInfo,
}