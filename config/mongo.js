const mongoose  = require('mongoose');
// make bluebird default Promise
const Promise   = require('bluebird');
const util      = require('util');
const debug     = require('debug');
const config    = require('./config');

// plugin bluebird promise in mongoose
mongoose.Promise = Promise;
// mongoose.set('useFindAndModify', false);
// connect to mongo db
mongoose.connect(config.mg_uri, 
    {
        useCreateIndex: true,
        useNewUrlParser: true,
        useFindAndModify: false,
        poolSize: 100,
        keepAlive: 1,
        autoReconnect: 1
    }
);
mongoose.connection.on('error', () => {
    throw new Error(`unable to connect to database: ${config.mg_uri}`);
});

// print mongoose logs in dev env
if (config.env === 'development') {
    mongoose.set('debug', (collection, method, query, doc) => {
        debug(`${collection}.${method}`, util.inspect(query, false, 20), doc);
    });
}

module.exports = mongoose;