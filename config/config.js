require('dotenv').config();//instatiate environment variables

let CONFIG = {} //Make this global to use all over the application

CONFIG.app_name = process.env.APP_NAME || 'Apps';
CONFIG.env      = process.env.ENV || 'development';
CONFIG.port     = process.env.PORT || '3000';

CONFIG.mg_host      = process.env.MG_HOST || 'localhost';
CONFIG.mg_port      = process.env.MG_PORT || '27017';
CONFIG.mg_dbname    = process.env.MG_DBNAME || 'name';
CONFIG.mg_user      = process.env.MG_USER || 'root';
CONFIG.mg_password  = process.env.MG_PASSWORD || 'password-please-change';
CONFIG.mg_uri       = `mongodb://${CONFIG.mg_user}:${CONFIG.mg_password}@${CONFIG.mg_host}:${CONFIG.mg_port}/${CONFIG.mg_dbname}`;

CONFIG.jwt_encryption = process.env.JWT_ENCRYPTION || 'jwt-please-change';
CONFIG.jwt_encryption_refresh = process.env.JWT_ENCRYPTION_REFRESH || 'jwt-refresh-please-change';
// one day
CONFIG.jwt_expiration = (process.env.JWT_EXPIRATION || 60) * 60;
CONFIG.jwt_expiration_refresh = (process.env.JWT_EXPIRATION_REFRESH || 60) * 60;

CONFIG.permissions = {};
CONFIG.permissions.ADMIN = process.env.AUTH_PERMISSION_ADMIN || 1;
CONFIG.permissions.USER = process.env.AUTH_PERMISSION_USER || 2;

module.exports = CONFIG;
